=========================================================================
fewColors: Color Image with Few Colors from an image with Labeled Objects
=========================================================================

Brief Description
=================

``fewColors`` allows to create a color image from an image with labeled objects so that adjacent objects have different colors while using the fewest colors possible.

.. |image| image:: https://gitlab.inria.fr/edebreuv/fewcolors/-/raw/master/test/input.png
   :width: 150%

.. |i-caption| replace:: *Image with labeled objects (background=0, objects=1, 2...).*

.. |result| image:: https://gitlab.inria.fr/edebreuv/fewcolors/-/raw/master/test/output.png
   :width: 150%

.. |r-caption| replace:: *Color image with the fewest colors possible.*

+-------------+-------------+
| |image|     | |result|    |
+-------------+-------------+
| |i-caption| | |r-caption| |
+-------------+-------------+



.. _installation:

Installation
============

The ``fewColors`` project is published on the `Python Package Index (PyPI) <https://pypi.org>`_ at: `https://pypi.org/project/few-colors <https://pypi.org/project/few-colors>`_. It requires version 3.8, or newer, of the interpreter. It should be installable from Python distribution platforms or Integrated Development Environments (IDEs). Otherwise, it can be installed from a command-line console:

- For all users, after acquiring administrative rights:
    - First installation: ``pip install few-colors``
    - Installation update: ``pip install --upgrade few-colors``
- For the current user (no administrative rights required):
    - First installation: ``pip install --user few-colors``
    - Installation update: ``pip install --user --upgrade few-colors``



Documentation
=============

After installation, the ``few-colors`` and ``few-colors-gui`` commands should be available from a command-line console.



Text-based Interface
--------------------

The usage help for ``few-colors`` is obtained with ``few-colors --help`` (see output below). The accepted file formats for the input labeled image are the image formats read by `imageio <https://github.com/imageio/imageio>`_, and the `NPY <https://numpy.org/doc/stable/reference/generated/numpy.save.html>`_ and `NPZ <https://numpy.org/doc/stable/reference/generated/numpy.savez_compressed.html>`_ Numpy formats. (See note below for the reason to use Numpy formats.) A labeled image or Numpy array must have the background labeled with zero, with the objects labeled consecutively from 1. The accepted image formats for the output color image are the image formats written by ``imageio``.

.. note::
    **With 8-bit image formats**, an image with labeled objects **cannot contain more than 255 objects**. Higher-depth formats allow to label more objects. However, it is recommended to use NPY or NPZ Numpy formats instead. Note that using Numpy arrays does not remove the limit on the number of objects, but with the `uint64 dtype <https://numpy.org/doc/stable/reference/arrays.scalars.html#numpy.uint64>`_, the limit goes up to close to 2e19 objects.

Usage Help::

    usage: few-colors [-h] file file

    Convert a labeled image or a labeled Numpy array into a minimally colored image

    positional arguments:
      file        Input labeled image or Numpy array and Output minimally colored image (pass input first)

    optional arguments:
      -h, --help  show this help message and exit



Graphical User Interface
------------------------

The command ``few-colors-gui`` can be launched with or without an argument as an initial image. If no argument is passed, a file browser opens to select an image. The color version is immediately displayed. It is then possible to change the image, change the object colors (individually or collectively; Press the ``?`` button for help), and save the color image. There is no ``close`` button. The window can simply be closed using the appropriate window manager button or keyboard shortcut.



Thanks
======

The project is developed with `PyCharm Community <https://www.jetbrains.com/pycharm>`_.

The development relies on several open-source packages (see ``install_requires`` in ``setup.py``).

The code is formatted by `Black <https://github.com/psf/black>`_, *The Uncompromising Code Formatter*.

The imports are ordered by `isort <https://github.com/timothycrosley/isort>`_... *your imports, so you don't have to*.
